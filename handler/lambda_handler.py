import os
import psycopg2
from functions import reader, writer
import json

db_user = os.environ['DB_USER']
db_passwd = os.environ['DB_USER_PASSWD']
db_name = os.environ['DB_NAME']
db_host = None


def select_host(method):
    hosts = {
        'GET': os.environ['DB_HOST_WRITER'],
        'POST': os.environ['DB_HOST_READER']
    }

    return hosts.get(method, None)


def lambda_handler(event, context):
    response_object = {}

    global db_host
    db_host = select_host(event['httpMethod'])

    if db_host is None:
        response_object['statusCode'] = 400
        response_object['headers'] = {}
        response_object['headers']['Content-type'] = 'application/json'
        response_object['body'] = json.dumps('Bad Request')

        return response_object

    connection = None

    try:
        connection = psycopg2.connect(f'host={db_host} dbname={db_name} user={db_user} password={db_passwd}')
        cursor = connection.cursor()
        http_method = event['httpMethod']

        if event['path'] == '/users/approximations':

            if http_method == 'GET':
                response_object = reader.get_user_approximations(event['queryStringParameters']['user'], cursor)

            if http_method == 'POST':
                response_object = writer.save_approximation(event['body'], event['queryStringParameters']['user'],
                                                            cursor)

        if event['path'] == '/users':
            if http_method == 'POST':
                response_object = writer.signup_user(event['body'], cursor)

    except psycopg2.DatabaseError:
        response_object['statusCode'] = 503
        response_object['headers'] = {}
        response_object['headers']['Content-type'] = 'application/json'
        response_object['body'] = json.dumps('Service Unavailable')
    finally:
        if connection is not None:
            connection.close()

    return response_object
