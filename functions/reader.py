import json
import psycopg2
import re


def get_user_approximations(user=None, cursor=None):
    response_object = {}
    try:
        cursor.execute(f"SELECT get_user_approximations('{user}')")
        response_object['statusCode'] = 200
        response_object['headers'] = {}
        response_object['headers']['Content-type'] = 'application/json'

        body = json.dumps(cursor.fetchall())
        body = re.sub(r'[\\\[\]()]', '', body)
        body = re.sub(r'\"{2}', '"', body)
        body = re.sub(r'\"{', '{', body)
        body = re.sub(r'{{2}', '{', body)
        body = re.sub('}\"', '}', body)
        body = re.sub('}{2}', '}', body)
        body = re.sub('"""', '"', body)
        body = re.sub(r'},{', '}{', body)
        body = re.sub(r'}{', '}#{', body)

        list_body = body.split('#')

        response_object['body'] = json.dumps(list_body)

        return response_object

    except psycopg2.DatabaseError as err:
        print(err)
        response_object['statusCode'] = 500
        response_object['headers'] = {}
        response_object['headers']['Content-type'] = 'application/json'
        response_object['body'] = json.dumps('Internal Server Error')

    return response_object
