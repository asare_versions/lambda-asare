import json
import psycopg2


def save_approximation(approximation=None, user=None, cursor=None):
    response_object = {}
    try:
        cursor.execute(f"SELECT save_approximation('{approximation}', '{user}')")
        response_object['statusCode'] = 200
        response_object['headers'] = {}
        response_object['headers']['Content-type'] = 'application/json'
        response_object['body'] = json.dumps(f'{approximation}\n Ok')
    except psycopg2.DatabaseError:
        response_object['statusCode'] = 500
        response_object['headers'] = {}
        response_object['headers']['Content-type'] = 'application/json'
        response_object['body'] = json.dumps({'Response': 'Internal Server Error',
                                              'Content': approximation})
    finally:
        cursor.close()

    return response_object


def signup_user(user, cursor):
    response_object = {}

    try:
        cursor.execute(f"SELECT signup_user('{user}')")
        response_object['statusCode'] = 200
        response_object['headers'] = {}
        response_object['headers']['Content-type'] = 'application/json'
        response_object['body'] = json.dumps('Ok')
    except psycopg2.DatabaseError:
        response_object['statusCode'] = 500
        response_object['headers'] = {}
        response_object['headers']['Content-type'] = 'application/json'
        response_object['body'] = json.dumps('Internal Server Error')
    finally:
        cursor.close()
    return response_object
