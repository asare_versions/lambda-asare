import json
import unittest
import psycopg2
import os
import functions.reader as reader
import functions.writer as writer


class TestLambda(unittest.TestCase):

    @classmethod
    def tearDownClass(cls) -> None:
        print('Tier-down')

    @classmethod
    def setUpClass(cls) -> None:
        print('Setup-Class')

    def setUp(self):
        db_test_host = os.environ['DB_TEST_HOST']
        db_test_user = os.environ['DB_TEST_USER']
        db_test_db = os.environ['DB_TEST_NAME']
        db_test_passwd = os.environ['DB_TEST_PASSWD']

        self.connection = psycopg2.connect(f'host={db_test_host} dbname={db_test_db} user={db_test_user} '
                                           f'password={db_test_passwd}')
        self.cursor = self.connection.cursor()

    def tearDown(self):
        self.connection.close()
        self.cursor.close()

    def test_get_user_approximations(self):
        test_user = json.dumps({'email': 'nullam@outlook.org'})
        response_object = reader.get_user_approximations(test_user, self.cursor)

        self.assertEqual(200, response_object['statusCode'])

    def test_save_approximation(self):
        test_approximation = '{"name":"Test", "records":[{"name":"SomeMaterial","unitCost":19.70,' \
                             '"description":"Huevos","amount":3,"recordType":"MATERIAL"},' \
                             '{"name":"SomeService","unitCost":48.70, "description":"Huevos2","amount":5,' \
                             '"recordType":"SERVICE"}], "totalCost":2000.50,"nMaterials":1,"nServices":1,' \
                             '"description":"Some Approximation","dateCreation":"2022-10-06 10:55:09"}'
        test_user = "{\"name\":\"JuCa\",\"email\":\"huevos@gmail.com\",\"password\":\"huevos\",\"privileges\":{ \
            \"Statement\": [ \
            { \
                \"Effect\": \"Allow\", \
                \"Action\": [ \
                    \"approximation:CREATE\", \
                    \"approximation:READ\", \
                    \"approximation:UPDATE\", \
                    \"approximation:DELETE\", \
                    \"records:CREATE\", \
                    \"records:READ\", \
                    \"records:UPDATE\",\
                    \"records:DELETE\",\
                    \"sub_users:CREATE\",\
                    \"sub_users:READ\",\
                    \"sub_users:UPDATE\",\
                    \"sub_users:DELETE\"\
                    ]\
                }\
            ]\
            }" \
                    "}"

        response_object = writer.save_approximation(test_approximation, test_user, self.cursor)
        self.assertEqual(response_object['statusCode'], 200)


if __name__ == '__main__':
    unittest.main()
